package Admin;

import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestParam;

import com.user.userPojo;

@Component
public class EmployeeRegi {

	@Value("${admin}")
	String username;
	@Value("${pass}")
	String pass;

	@Autowired
	JdbcTemplate jdbcTemplate;

	public LoginPojo login(LoginPojo loginDetails) {
		List<LoginPojo> getAdmin = jdbcTemplate.query(
				" Select fullName, email, password, activate, role from admininfo where email = '"
						+ loginDetails.getEmail() + "' && password= '" + loginDetails.getPassword() + "'",
				new BeanPropertyRowMapper(LoginPojo.class));

		if (getAdmin.isEmpty()) {
			List<LoginPojo> getEmployee = jdbcTemplate.query(
					" Select fullName, email, password, activate, role from employeelist where email = '"
							+ loginDetails.getEmail() + "' && password= '" + loginDetails.getPassword() + "'",
					new BeanPropertyRowMapper(LoginPojo.class));

			if (getEmployee.isEmpty()) {
				List<LoginPojo> getDistributer = jdbcTemplate.query(
						" Select id, email, password, activate, role, distributer from distributerlist where email = '"
								+ loginDetails.getEmail() + "' && password= '" + loginDetails.getPassword() + "'",
						new BeanPropertyRowMapper(LoginPojo.class));
				return getDistributer.get(0);
			} else {
				return getEmployee.get(0);
			}

		} else {
			return getAdmin.get(0);
		}

	}

	// employee register

	public int employeeRegi(EmployeePojo employeeInfo) {

		List<EmployeePojo> getData = jdbcTemplate.query(
				" Select * from employeelist where id = '" + employeeInfo.getId() + "'",
				new BeanPropertyRowMapper(EmployeePojo.class));
		if (getData.isEmpty()) {
			jdbcTemplate.update(
					"insert into employeelist (fullName, email, accessBlocks, password, activate, role) values (?,?,?,?,?,?)",
					new Object[] { employeeInfo.getFullName(), employeeInfo.getEmail(), employeeInfo.getAccessBlocks(),
							employeeInfo.getPassword(), employeeInfo.getActivate(), employeeInfo.getRole() });
		} else {
			jdbcTemplate.update(
					"update employeelist set fullName = ?, email = ?, accessBlocks = ?, password = ?, activate = ?, role = ? where id = ?",
					employeeInfo.getFullName(), employeeInfo.getEmail(), employeeInfo.getAccessBlocks(),
					employeeInfo.getPassword(), employeeInfo.getActivate(), employeeInfo.getRole(),
					employeeInfo.getId());
		}

		return 1;

	}

	public int distributerRegi(DistributerPojo distributerInfo) {

		List<DistributerPojo> getData = jdbcTemplate.query(
				" Select * from distributerlist where id = '" + distributerInfo.getId() + "'",
				new BeanPropertyRowMapper(DistributerPojo.class));
		if (getData.isEmpty()) {
			jdbcTemplate.update(
					"insert into distributerlist (distributer, email, password, activate, role) values (?, ?, ?, ?, ?)",
					new Object[] { distributerInfo.getDistributer(), distributerInfo.getEmail(),
							distributerInfo.getPassword(), distributerInfo.getActivate(), distributerInfo.getRole() });
		} else {
			jdbcTemplate.update(
					"update distributerlist set distributer = ?, email = ?, password = ?, activate = ?, role = ? where id = ?",
					distributerInfo.getDistributer(), distributerInfo.getEmail(), distributerInfo.getPassword(),
					distributerInfo.getActivate(), distributerInfo.getRole(), distributerInfo.getId());
		}

		return 1;

	}

	public int productRegi(ProductPojo productInfo) {

		List<ProductPojo> getData = jdbcTemplate.query(
				" Select * from productlist where id = '" + productInfo.getId() + "'",
				new BeanPropertyRowMapper(ProductPojo.class));
		if (getData.isEmpty()) {
			jdbcTemplate.update("insert into productlist (productName) values (?)",
					new Object[] { productInfo.getProductName() });
		} else {
			jdbcTemplate.update("update productlist set productName = ? where id = ?", productInfo.getProductName(),
					productInfo.getId());
		}

		return 1;

	}

	public int accessBlockRegi(AccessBlocksPojo accessBlockInfo) {

		List<AccessBlocksPojo> getData = jdbcTemplate.query(
				" Select * from accessblockslist where id = '" + accessBlockInfo.getId() + "'",
				new BeanPropertyRowMapper(EmployeePojo.class));
		if (getData.isEmpty()) {
			jdbcTemplate.update("insert into accessblockslist (accessBlock) values (?)",
					new Object[] { accessBlockInfo.getAccessBlock() });
		} else {
			jdbcTemplate.update("update accessblockslist set accessBlock = ? where id = ?",
					accessBlockInfo.getAccessBlock(), accessBlockInfo.getId());
		}

		return 1;

	}

	public List<EmployeePojo> getEmployeeList() {
		List<EmployeePojo> employee = jdbcTemplate.query("select * from employeelist",
				new BeanPropertyRowMapper(EmployeePojo.class));
		return employee;
	}

	public List<DistributerPojo> getDistributerList() {
		List<DistributerPojo> distributer = jdbcTemplate.query("select * from distributerlist",
				new BeanPropertyRowMapper(DistributerPojo.class));
		return distributer;
	}

	public List<ProductPojo> getProductList() {
		List<ProductPojo> product = jdbcTemplate.query("select * from productlist",
				new BeanPropertyRowMapper(ProductPojo.class));
		return product;
	}

	public List<AccessBlocksPojo> getAccessBlockList() {
		List<AccessBlocksPojo> accessBlock = jdbcTemplate.query("select * from accessblockslist",
				new BeanPropertyRowMapper(AccessBlocksPojo.class));
		return accessBlock;
	}

}
