package Admin;

import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AdminController {

	@Autowired
	JdbcTemplate jdbcTemplate;

	@Autowired
	EmployeeRegi EmployeeRegi;

	@Value("${pass}")
	String pass;
	
	@RequestMapping("/home")
	@CrossOrigin
	public LoginPojo login(@RequestBody LoginPojo loginDetails) {

		return EmployeeRegi.login(loginDetails);
	}	

	@RequestMapping("/addEmployee")
	@CrossOrigin
	public int saveEmployee(@RequestBody EmployeePojo employeeInfo) {

		return EmployeeRegi.employeeRegi(employeeInfo);
	}

	@RequestMapping("/addDistributer")
	@CrossOrigin
	public int saveDistributer(@RequestBody DistributerPojo distributerInfo) {

		return EmployeeRegi.distributerRegi(distributerInfo);
	}

	@RequestMapping("/addProduct")
	@CrossOrigin
	public int saveProduct(@RequestBody ProductPojo productInfo) {

		return EmployeeRegi.productRegi(productInfo);
	}

	@RequestMapping("/addAccessBlock")
	@CrossOrigin
	public int saveAccessBlock(@RequestBody AccessBlocksPojo accessBlockInfo) {

		return EmployeeRegi.accessBlockRegi(accessBlockInfo);
	}

	@RequestMapping("/getEmployee")
	@CrossOrigin
	public List<EmployeePojo> getEmployeeList() {

		return EmployeeRegi.getEmployeeList();
	}

	@RequestMapping("/getDistributer")
	@CrossOrigin
	public List<DistributerPojo> getDistributerList() {

		return EmployeeRegi.getDistributerList();
	}

	@RequestMapping("/getProduct")
	@CrossOrigin
	public List<ProductPojo> getProductList() {

		return EmployeeRegi.getProductList();
	}

	@RequestMapping("/getAccessBlock")
	@CrossOrigin
	public List<AccessBlocksPojo> getAccessBlockList() {

		return EmployeeRegi.getAccessBlockList();
	}

}
