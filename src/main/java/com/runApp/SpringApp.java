package com.runApp;

import javax.sql.DataSource;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.scheduling.annotation.EnableScheduling;

@Configuration
@EnableAutoConfiguration
@SpringBootApplication
@EnableScheduling
@ComponentScan(value = { "Admin" })
@ComponentScan(value = { "com.user" })
public class SpringApp {

	public static void main(String args[]) {

		SpringApplication.run(SpringApp.class, args);

	}

}
